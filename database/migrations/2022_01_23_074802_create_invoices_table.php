<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->date('tanggal_invoice');
            $table->date('tanggal_kirim');
            $table->string('costumer', 200);
            $table->string('nama_projek', 250);
            $table->integer('no_invoice');
            $table->integer('no_faktur');
            $table->integer('termin');
            $table->integer('nominal_projek');
            $table->integer('ppn');
            $table->integer('total_invoice');
            $table->date('keterangan', 200);
            $table->string('pembayaran', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
