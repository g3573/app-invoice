@extends('layouts.home')

@section('content')
{{-- @dump(get_defined_vars()) --}}
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add new</h2>
            </div>
        </div>
    </div>
    <br>  
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form action="{{ route('pengirimans.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>Tanggal Invoice</strong>
                    <input type="date" name="tanggal_invoice" class="form-control" placeholder="Tanggal Invoice">
                </div>
                <div class="form-group">
                    <strong>Nama Projek</strong>
                    <select class="form-control" name="nama_projek" id="namaProjek">
                        @foreach($products as $product)
                            <option value="{{$product->nama_projek}}" data-costumer="{{$product->costumer}}">{{$product->nama_projek}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <strong>No Invoice</strong>
                    <input type="number" name="no_invoice" class="form-control" placeholder="No Invoice">
                </div>
                <div class="form-group">
                    <strong>Termin</strong>
                    <input type="number" name="termin" class="form-control" placeholder="Termin">
                </div>
                <div class="form-group">
                    <strong>Nominal</strong>
                    <input type="number" name="nominal_projek" class="form-control" placeholder="Nominal">
                </div>
                <div class="form-group">
                    <strong>Keterangan</strong>
                    <input type="date" name="keterangan" class="form-control" placeholder="Keterangan">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>Tanggal Kirim</strong>
                    <input type="date" name="tanggal_kirim" class="form-control" placeholder="Tanggal Kirim">
                </div>
                <div class="form-group">
                    <strong>Costumer</strong>
                    <input type="text" name="costumer" class="form-control" placeholder="costumer" id="costumersInput" readonly>
                </div>
                <div class="form-group">
                    <strong>No Faktur</strong>
                    <input type="number" name="no_faktur" class="form-control" placeholder="No Faktur">
                </div>
                <div class="form-group">
                    <strong>Total Invoice</strong>
                    <input type="number" name="total_invoice" class="form-control" placeholder="Total Invoice">
                </div>
                <div class="form-group">
                    <strong>PPN</strong>
                    <input type="number" name="ppn" class="form-control" placeholder="PPN">
                </div>
                <div class="form-group">
                    <strong>Pembayaran</strong>
                    <input type='currency' name="pembayaran" class="form-control" placeholder="Pembayaran">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <a class="btn btn-danger" href="{{ route('pengirimans.index') }}"> Back</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
@endsection

@section('javascripts')
    <script>
        // code untuk mengisi collum costumer
        // saat memilih project
        let namaProjek = document.getElementById('namaProjek');
        let costumersInput = document.getElementById('costumersInput');

        namaProjek.addEventListener('change', (e) => {
            // console.log( namaProjek.options[namaProjek.selectedIndex].dataset);
            let costumer = namaProjek.options[namaProjek.selectedIndex].dataset.costumer;
            costumersInput.value=costumer;
        })
    </script>
@endsection