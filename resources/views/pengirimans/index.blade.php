@extends('layouts.home')

@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Invoice</h1>
        <a href="{{ route('pengirimans.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i
                class="fas fa-plus fa-sm text-white-50"></i> Tambah Invoice</a>
    </div>
    <br>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Table Invoice</h6>
        </div>
        <div class="card-body">
                <table class="display table table-responsive" id="table_id" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Invoice</th>
                            <th>Tanggal Kirim</th>
                            <th>Costumer</th>
                            <th>Nama Projek</th>
                            <th>No Invoice</th>
                            <th>No Faktur</th>
                            <th>Termin</th>
                            <th>Nominal Pajak</th>
                            <th>PPN</th>
                            <th>Total Invoice</th>
                            <th>Keterangan</th>
                            <th>Pembayaran</th>
                            <th width="280px">action</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pengirimans as $pengiriman)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $pengiriman->tanggal_invoice }}</td>
                            <td>{{ $pengiriman->tanggal_kirim}}</td>
                            <td>{{ $pengiriman->costumer }}</td>
                            <td>{{ $pengiriman->nama_projek }}</td>
                            <td>{{ $pengiriman->no_invoice }}</td>
                            <td>{{ $pengiriman->no_faktur }}</td>
                            <td>{{ $pengiriman->termin }}</td>
                            <td>{{ $pengiriman->nominal_projek }}</td>
                            <td>{{ $pengiriman->ppn }}</td>
                            <td>{{ $pengiriman->total_invoice }}</td>
                            <td>{{ $pengiriman->keterangan }}</td>
                            <td>{{ $pengiriman->pembayaran }}</td>
                            <td>
                                <a class="btn btn-circle btn-primary" href="{{ route('pengirimans.edit',$pengiriman->id) }}">
                                    <i class="fas fa-edit fa-md"></i></a>
                            </td>
                            <td>
                                <button type="submit" class="btn btn-circle btn-danger" data-toggle="modal" data-target="#deleteModal">
                                    <i class="fas fa-trash fa-md"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
    <!-- Delete Modal-->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin ingin menghapus data ini</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Anda tidak akan bisa memulihkan kembali data ini</div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
                    @foreach ($pengirimans as $pengiriman)
                        <form action="{{ route('pengirimans.destroy',$pengiriman->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            @endforeach
                            <button type="submit" class="btn btn-danger">
                                Delete
                            </button>
                        </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
@endsection