@extends('layouts.home')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add new</h2>
            </div>
        </div>
    </div>
    <br>  
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        
    <form action="{{ route('projects.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>No Projek</strong>
                    <input type="number" name="no_projek" class="form-control" placeholder="No Projek">
                </div>
                <div class="form-group">
                    <strong>No SPK</strong>
                    <input type="number" name="no_spk" class="form-control" placeholder="No SPK">
                </div>
                <div class="form-group">
                    <strong>Costumer</strong>
                    <input type="text" name="costumer" class="form-control" placeholder="Costumer">
                </div>
                <div class="form-group">
                    <strong>Nama Projek</strong>
                    <input type="text" name="nama_projek" class="form-control" placeholder="Nama Projek">
                </div>
                <div class="form-group">
                    <strong>Tanggal Mulai</strong>
                    <input type="date" name="tanggal_mulai" class="form-control" placeholder="Tanggal Mulai">
                </div>
                <div class="form-group">
                    <strong>Tanggal Akhir</strong>
                    <input type="date" name="tanggal_akhir" class="form-control" placeholder="Tanggal Akhir">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <a class="btn btn-danger" href="{{ route('projects.index') }}"> Back</a>
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>
        
    </form>
@endsection