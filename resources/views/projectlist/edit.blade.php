@extends('layouts.home')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit</h2>
            </div>
        </div>
    </div>
    <br>  
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        
    <form action="{{ route('projects.update', $project->id) }}" method="POST" enctype="multipart/form-data"> 
        @csrf

        @method('PUT')
        
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>No Projek</strong>
                    <input type="number" name="no_projek" class="form-control" placeholder="No Projek" value="{{$project->no_projek}}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>No SPK</strong>
                    <input type="number" name="no_spk" class="form-control" placeholder="No SPK" value="{{$project->no_spk}}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>Costumer</strong>
                    <input type="text" name="costumer" class="form-control" placeholder="Costumer" value="{{$project->costumer}}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>Nama Projek</strong>
                    <input type="text" name="nama_projek" class="form-control" placeholder="Nama Projek" value="{{$project->nama_projek}}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>Tanggal Mulai</strong>
                    <input type="date" name="tanggal_mulai" class="form-control" placeholder="Tanggal Mulai" value="{{$project->tanggal_mulai}}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>Tanggal Akhir</strong>
                    <input type="date" name="tanggal_akhir" class="form-control" placeholder="Tanggal Akhir" value="{{$project->tanggal_akhir}}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <a class="btn btn-danger" href="{{ route('projects.index') }}"> Back</a>
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>
        
    </form>
@endsection