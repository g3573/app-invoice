<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Selamat datang</title>
    <meta name="description" content="Prism is a beautiful Bootstrap 4 template for open-source landing pages."/>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=K2D:300,400,500,700,800" rel="stylesheet">

    <!-- Bootstrap CSS / Color Scheme -->
    <link rel="stylesheet" href="{{ asset('assets/prism/css/bootstrap.min.css') }}">
</head>
<body>

<!--Header Section-->
<section class="bg-secondary pt-5 pb-6">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex flex-row align-items-center justify-content-between text-white">
                <div class="heading-brand">Finance</div>
                @if (Route::has('login'))
                @auth
                <a href="{{ url('projects') }}" class="btn btn-light">Dashboard</a>
            </div>
            <div class="col-12 d-flex flex-row align-items-center justify-content-between text-white">
                @else
                <a href="{{ route('login') }}" class="btn btn-light">Login</a>
            </div>
        </div>
        <div class="row mt-6">
            <div class="col-md-8 mx-auto text-center">
                <h1 class="text-white">Permudah segala kebutuhan finansial anda bersama kami</h1>
                    @if (Route::has('register'))
                <a href="{{ route('register') }}" class="btn btn-success svg-icon">
                    <em class="mr-2"></em>
                    Register
                </a>
                @endif
                @endif
            </div>
            @endif
        </div>
        <div class="row mt-5">
            <div class="col-md-9 mx-auto">
                <div class="code-window">
                    <div class="dots">
                        <div class="red"></div>
                        <div class="orange"></div>
                        <div class="green"></div>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <img src="{{asset('assets/admin/img/undraw_mobile_payments_re_7udl.svg')}}">
                    </div>
                </div>
            </div>
        </div>
</section>

<!--Features Section-->
<section class="py-6">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <h2 class="text-center text-md-left">Apa Saja yang Anda Bisa Lakukan <span class="text-success">Bersama Kami</span></h2>
                <div class="row feature-grid">
                    <div class="col-sm-6">
                        <div class="media">
                            <div class="icon-box">
                                <div class="icon-box-inner">
                                    <span data-feather="list" width="28" height="28"></span>
                                </div>
                            </div>
                            <div class="media-body">
                                Membuat Daftar Projek
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="media">
                            <div class="icon-box">
                                <div class="icon-box-inner">
                                    <span data-feather="dollar-sign" width="28" height="28"></span>
                                </div>
                            </div>
                            <div class="media-body">
                                Menghitung Hutang
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="media">
                            <div class="icon-box">
                                <div class="icon-box-inner">
                                    <span data-feather="feather" width="28" height="28"></span>
                                </div>
                            </div>
                            <div class="media-body">
                                Menu Piutang
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="media">
                            <div class="icon-box">
                                <div class="icon-box-inner">
                                    <span data-feather="printer" width="28" height="28"></span>
                                </div>
                            </div>
                            <div class="media-body">
                                Membuat Invoice
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="media">
                            <div class="icon-box">
                                <div class="icon-box-inner">
                                    <span data-feather="menu" width="28" height="28"></span>
                                </div>
                            </div>
                            <div class="media-body">
                                Menu Purchase Order
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--footer-->
<footer class="py-5 bg-light">
        <div class="row my-2">
            <div class="col-md-4 mx-auto text-muted text-center small-xl">
                &copy; 2020 Your company - All Rights Reserved
            </div>
        </div>
    </div>
</footer>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.7.3/feather.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/prism.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/plugins/line-numbers/prism-line-numbers.min.js"></script>
<script src="{{ asset('assets/prism/js/scripts.js') }}"></script>
</body>
</html>
