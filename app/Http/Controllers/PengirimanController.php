<?php

namespace App\Http\Controllers;

use App\Models\Pengiriman;
use App\Models\Project;
use Illuminate\Http\Request;
use Alert;

class PengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengirimans = Pengiriman::all();

        return view('pengirimans.index',compact('pengirimans'))
            ->with('i', (request()->input('page', 1) - 1) * 5);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $products = Project::all();
        return view('pengirimans.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tanggal_invoice' => 'required', 
            'tanggal_kirim' => 'required', 
            'costumer' => 'required', 
            'nama_projek' => 'required', 
            'no_invoice' => 'required', 
            'no_faktur' => 'required',
            'termin' => 'required', 
            'nominal_projek' => 'required', 
            'ppn' => 'required',
            'total_invoice' => 'required', 
            'keterangan' => 'required', 
            'pembayaran' => 'required',
        ]);

        Pengiriman::create($request->all());
        toast('Berhasil menambah!','success');
        return redirect()->route('pengirimans.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pengiriman  $pengiriman
     * @return \Illuminate\Http\Response
     */
    public function show(Pengiriman $pengiriman)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pengiriman  $pengiriman
     * @return \Illuminate\Http\Response
     */
    public function edit(Pengiriman $pengiriman)
    {   
        $products = Project::all();
        return view('pengirimans.edit',compact('pengiriman', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pengiriman  $pengiriman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pengiriman $pengiriman)
    {
        $request->validate([
            'tanggal_invoice' => 'required', 
            'tanggal_kirim' => 'required', 
            'costumer' => 'required', 
            'nama_projek' => 'required', 
            'no_invoice' => 'required', 
            'no_faktur' => 'required',
            'termin' => 'required', 
            'nominal_projek' => 'required', 
            'ppn' => 'required',
            'total_invoice' => 'required', 
            'keterangan' => 'required', 
            'pembayaran' => 'required',
        ]);

        $pengiriman->update($request->all());
        toast('Berhasil mengubah!','success');
        return redirect()->route('pengirimans.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pengiriman  $pengiriman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pengiriman $pengiriman)
    {
        $pengiriman->delete();

        toast('Berhasil mengahapus!','success');
        return redirect()->route('pengirimans.index');
    }
}
