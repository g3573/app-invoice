<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
    use HasFactory;

    protected $table = 'invoices';
    protected $fillable = [
        'tanggal_invoice', 'tanggal_kirim', 'costumer', 
        'nama_projek', 'no_invoice', 'no_faktur',
        'termin', 'nominal_projek', 'ppn',
        'total_invoice', 'keterangan', 'pembayaran'
    ];
}
