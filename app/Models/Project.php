<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $table = 'daftar_produk';
    protected $fillable = ['no_projek', 'no_spk', 'costumer', 'nama_projek', 'tanggal_mulai', 'tanggal_akhir'];
}
